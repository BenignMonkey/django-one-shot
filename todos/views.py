from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.forms import TodoListForm
# Create your views here.

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos
    }

    return render(request, "todos/todos.html", context)

def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        'todo_list_detail': todo_list_detail,
    }
    return render(request, 'todos/detail.html', context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', list.id)
    else:
        form = TodoListForm()
        context = {
            "form": form,
            }
    return render(request, 'todos/create.html', context)
